package com.springboot.h2qulbi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.h2qulbi.entity.Hardware;


/**
 * @author fascal
 */
public interface HardwareRepository extends JpaRepository<Hardware, Long> {

}
