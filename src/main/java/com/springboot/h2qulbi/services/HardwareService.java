package com.springboot.h2qulbi.services;

import java.util.List;

import com.springboot.h2qulbi.entity.Hardware;


public interface HardwareService {

    List<Hardware> findAll();

    Hardware findById(Long id);

    Hardware update(Long id, Hardware hardware);

    Hardware create(Hardware hardware);
    
    void delete(Long id);
}