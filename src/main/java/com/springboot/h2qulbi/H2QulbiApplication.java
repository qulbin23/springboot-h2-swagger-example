package com.springboot.h2qulbi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class H2QulbiApplication {

	public static void main(String[] args) {
		SpringApplication.run(H2QulbiApplication.class, args);
	}

}
